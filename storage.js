/**
 * initializes local storage
 *
 * @return     {Boolean}  { True if local storage initialized, False otherwise }
 */
function initStorage() {

	if (!isLocalStorageAvailable())
		return false;
		
	var storageVS = localStorage.getItem("vs");
	var storageFS = localStorage.getItem("fs");

	if (storageVS !== null) {
		//vsEditor.setValue(storageVS);
	} else {
		localStorage.setItem("vs", vsEditor.getValue());
	}
	
	if (storageFS !== null) {
		//fsEditor.setValue(storageFS);
	} else {
		localStorage.setItem("fs", fsEditor.getValue());
	}
	
	return true;
}

/**
 * Determines if local storage is available.
 *
 * @return     {Boolean}  True if local storage available, False otherwise.
 */
function isLocalStorageAvailable() {
	try {
		var storage = window.localStorage;
        var x = 'localStorageTest';

		storage.setItem(x, x);
		storage.removeItem(x);
		return true;
	}
	catch (e) {
		return false;
	}
}